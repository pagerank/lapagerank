import java.io.*;

/**
 * Created by rishimittal on 2/4/14.
 */
public class ForwardLinkCount {

    private static String forwardLinkFile = LAPageRankMain.index_folder + LAPageRankMain.forwardIndex;
    private static String forwardLinkCountFile = LAPageRankMain.index_folder + LAPageRankMain.forwardLinkCount;


    public static void countInLinks(){

        File f = new File(forwardLinkFile);//Filepath of the link->outlinks file
        File fo = new File(forwardLinkCountFile);
        BufferedReader br = null;
        BufferedWriter brw = null;
        try {
            br = new BufferedReader(new FileReader(f));
            brw  = new BufferedWriter(new FileWriter(fo));
            String line ;
            br.readLine();
            while( ( line  = br.readLine()  )!= null){

                int colonIndex = line.indexOf(":");
                String pageid = line.substring(0, colonIndex);
                //System.out.print(pageid);
                String linkString = line.substring(colonIndex + 1 , line.length());
                int countLinks = linkString.length() - linkString.replace(",", "").length();
                //System.out.println( ":" + countLinks);
                brw.write(pageid + ":");
                brw.write(countLinks + "\n");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                br.close();
                brw.close();
            } catch (IOException e) {
                e.printStackTrace();
           }
        }

        System.out.println("Forward Link Counting Complete...!");
    }

}
