import java.io.*;

/**
 * Created by rishimittal on 3/4/14.
 */
public class CreateBackwardIndex {

    public static String index_folder = "/home/rishimittal/IRE_INDEX/";
    public static String backwardIndex = "backward-index.txt";
    public static String backwardIndex_index = "backward-index-index.txt";
    public static int OFFSET;

    public static void execute(){

        File ft = new File(index_folder + backwardIndex);
        File oft = new File(index_folder + backwardIndex_index);
        BufferedReader br = null;
        BufferedWriter brw = null;

        try {
            br = new BufferedReader(new FileReader(ft));
            brw = new BufferedWriter(new FileWriter(oft));
            int i = 0;
            String line;
            while( ( line = br.readLine()) != null ){
                String[] colonSplit = line.split(":");

                if(i == 0 ) {
                    brw.write(colonSplit[0] + ":0\n" );
                    //System.out.println(colonSplit[0] + ":0\n");
                    i++;
                    OFFSET = line.getBytes().length + 1;
                    continue;
                }

                brw.write(colonSplit[0]+":"+OFFSET+"\n");
                //System.out.println(colonSplit[0] + ":" + OFFSET + "\n");
                OFFSET += line.getBytes().length + 1;
                i++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                br.close();
                brw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        System.out.println("Backward Index Created..!");
    }
    public static void main(String arr[]){
        CreateBackwardIndex.execute();
    }

}
