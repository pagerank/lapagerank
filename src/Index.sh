#!/bin/bash

######################################################################
# This script extracts links from the wikipedia xml corpus 	     #
# and creates forward and backward index.                            #
#                                                                    #
# Input: XML File in MediaWiki Markup                                #
# Output:                                                            #
#    IdtoTitle.txt						     #
#    forward-index.txt			                             #
#    backward-index.txt (inverted index)                             #
#                                                                    #
# Author: Jigar Kaneriya                                             #
######################################################################

SCRIPT=$(basename $0)
FILENAME=$1

usage(){
    echo "USAGE: $BASENAME file.xml";
}

# Argparsing
while getopts "h" opt
do
    case $opt in

        h) usage;;
        *) usage;;

    esac
done

grep -e '^    <id>' "$FILENAME" | grep [0-9]* -o ids > ids # fetch ids from corpus
grep '<title>' sample.xml | sed -e 's/<\/title>//g' -e 's/<title>//g' -e 's/    //g' > titles #feth titles from corpus
paste ids titles -d' ' > IdtoTitle.txt # merge ids and titels line by line
rm ids titles # remove intermediate files
echo "Mapping file for 'ids to titles' created "
bash filter.sh "$FILENAME" | python title_to_id.py IdtoTitle.txt > forward-index.txt 2>/dev/null # creates forward index by parsing corpus
echo "Forward index created"
python invertIndex.py forward-index.txt #invert forward index
mv merged.txt backward-index.txt #rename merged inverted index
rm inverted_index_* #remove intermediate files
echo "backward index created"

