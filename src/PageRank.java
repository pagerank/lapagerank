import com.sun.org.apache.xpath.internal.SourceTree;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by rishimittal on 23/3/14.
 */

public class PageRank {

    private static int [] linkCount = new int[50000000];
    private static float [] pageRanks = new float[50000000];
    private static float [] oldPageRanks = new float[50000000];
    private static float [] tmp;
    private static int totalnumofpages = 14041179;
    private static float dampingFactor = 0.15f;
    private static int nextIteration=0;

    private static String forwardLinkCountFile = LAPageRankMain.index_folder + LAPageRankMain.forwardLinkCount;
    private static String backwardLinksFile = LAPageRankMain.index_folder + LAPageRankMain.backwardIndex;
    private static String pageRankFile = LAPageRankMain.index_folder + LAPageRankMain.globalPageRank;

    private static void loadLinksInArray(){

        File ft = new File(forwardLinkCountFile);
        BufferedReader br = null;
        String line;

        try {

            br = new BufferedReader(new FileReader(ft));

            while( (line = br.readLine()) != null ){
                //System.out.print(line);
                int colonIndex = line.indexOf(":");
                //System.out.print(": " + line.substring(0, colonIndex));
                //System.out.println(": " + line.substring(colonIndex + 1 , line.length()));
                int count = Integer.parseInt(line.substring(colonIndex + 1 , line.length()));
                if(count != 0 ) {
                    linkCount[Integer.parseInt(line.substring(0, colonIndex))] = count;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static boolean calculatePageRank(String line) {

        String [] lineSplit = line.split(":");
        int pageId = Integer.parseInt(lineSplit[0]);

        String [] pageSplit = lineSplit[1].split(",");

        float tempRank = 0;
        int tempFreq = 0;

        for(int i = 0 ; i  < pageSplit.length ; i++ ){

            Float tempRf = oldPageRanks[Integer.parseInt(pageSplit[i])];
            int freq = linkCount[Integer.parseInt(pageSplit[i])];

            if(tempRf == null){
                tempRank = (float) (tempRank +  1 / (float)freq );
            }else{
                tempRank = (float) ( tempRank + tempRf / (float)freq) ;
            }
        }
        //tempRank /= sum;
        float val = (float)(tempRank * (1 - dampingFactor) + dampingFactor );
        //if(nextIteration == 2)
          //  System.out.println(val);

        float oldRf = oldPageRanks[pageId];
        pageRanks[pageId] = val;
        float f = Math.abs(oldRf - val);

        if(f < 0.00005f)
            return true;
        else{
            return false;
        }
     }

    public static int  readandIterate(){

        File f = new File(backwardLinksFile); //filePath of the pageId->from links
        int count = 0;
        System.out.println(nextIteration++);
        try {
            BufferedReader br = new BufferedReader(new FileReader(f));
            String line ;
            while( ( line  = br.readLine() )!= null){
                //System.out.println(line);
                if(!calculatePageRank(line)){
                    count++;
                }
            }
            if(count <= 10000 ) return 0; // to be updated on the experiment basis
            //if(nextIteration == 4 ) return 0;
            System.out.println("Page Id Updated :" + count);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }

    public static void execute() {

        //System.out.println("Loding Forward Count In Memory .");
        PageRank.loadLinksInArray();

        int i = 1;

        int changedPageranks;

        while((changedPageranks = PageRank.readandIterate()) != 0 ){
            tmp = pageRanks;
            pageRanks = oldPageRanks;
            oldPageRanks = tmp;
        }
        System.out.println("Global Page Rank Calculated...Now writing to file");
        File fty = new File(pageRankFile);
        BufferedWriter bw = null;
        try {
            bw=new BufferedWriter(new FileWriter(fty));

            for( i = 0; i < 50000000; i++){
                float pr = oldPageRanks[i];
                if(pr != 0f )
                    bw.write( i + ":" +pr + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Global Page Ranks Calculated..!");
    }

}