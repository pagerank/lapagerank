import numpy
import sys
import merger

N = 55000000
entry_count = 0
file_count = 0
out_file_name = 'inverted_index_'

def write_to_file(inverted_index,output_file_name):
	""" writes dictionary to file wtih appending incremental integer to file name every time"""
	if not hasattr(write_to_file, "file_count"):
		write_to_file.file_count = 0  # it doesn't exist yet, so initialize it
	write_to_file.file_count+=1
        fd = open(output_file_name+str(write_to_file.file_count)+'.txt','w')
        for i,j in sorted(inverted_index.items()):
            fd.write(`i`+':')
            for k in j:
                fd.write(str(k)+',')
            fd.write('\n')
        inverted_index.clear()

if len( sys.argv ) < 2:
    print 'Error:Invalid arguments.\nUsage: '+sys.argv[0]+' <file_name>'
    sys.exit()



inverted_index = {}

for line in open(sys.argv[1],'r'):
    #print line,
    if ':' in line:
        pageid,outlinks = line.split(':')
        if outlinks != '' and pageid != '':
            pageid = int(pageid)
            for linkid in outlinks.split(','):
                if linkid != '\n' and linkid!='':
                    entry_count+=1
                    if inverted_index.has_key(int(linkid)):
                        inverted_index [ int(linkid) ].append(pageid)
                    else:
                        inverted_index [ int(linkid) ]=[pageid]
    if entry_count > N:
        write_to_file(inverted_index,out_file_name)
        entry_count = 0
write_to_file(inverted_index,out_file_name)

merger.merge(out_file_name,4)#write_to_file.file_count)

