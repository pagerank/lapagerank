/**
 * Created by rishimittal on 3/4/14.
 */
public class LAPageRankMain {


    public static String index_folder;
    public static String forwardLinkCount = "forwardCounts.txt";
    public static String forwardIndex = "forward-index.txt";
    public static String backwardIndex = "backward-index.txt";
    public static String globalPageRank = "globalPagerank.txt";


    public static void main(String arr[]){

        index_folder = arr[0];
        // Make the forward Link Count file which is used for the
        // calcuating the page rank.
        ForwardLinkCount.countInLinks();

        // Calculate the Pagerank and dump the non-zero values
        // int the "globalPageRank" file
        PageRank.execute();

        //Backward Index is created
        CreateBackwardIndex.execute();



    }
}
