#!/bin/bash

######################################################################
# This script extracts and converts titles and links from the        #
# wikipedia xml corpus.                                              #
#                                                                    #
# Input: XML File in MediaWiki Markup                                #
# Output:                                                            #
#    Titles of the page preceeded by 't:',                           #
#    Links preceeded by 'l:'                                         #
#                                                                    #
# Author: Anhad Jai Singh                                            #
######################################################################

SCRIPT=$(basename $0)
FILENAME=$1

usage(){
    echo "USAGE: $BASENAME file.xml";
}

# Argparsing
while getopts "h" opt
do
    case $opt in

        h) usage;;
        *) usage;;

    esac
done


grep -P '\[\[.*?\]\]|<title>.*?</title>' -o $FILENAME | # Get title and Links
grep -P -v -i "\[\[(Image|\w\w:|Simple:)" | # Remove images, language translation links,
sed 's/<title>\(.*\)<\/title>/t:\1/' | # change title tags to t: *actual title*
sed 's/\[\[\(.*\)\]\]/l:\1/' # change links to l: *actual link name*
