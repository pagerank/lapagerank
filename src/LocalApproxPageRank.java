
import java.io.*;
import java.lang.reflect.Array;
import java.util.*;

/**
 * Created by rishimittal on 1/4/14.
 */
public class LocalApproxPageRank {


    private static int totalnumofpages = 14041179;
    private static float pagerank[] = new float[50000000];

    private static Map<Integer, List<Integer>> back_index_map;
    private static Map<Integer, Integer> forward_count_map ;
    private static long [] back_index_index = new long[50000000];
    private static Map<Integer, List<Integer>> local_back_index_map;

    private static int radius = 3; // To be Modified
    private static int levelid = 0; //last level count
    private static int lastidvalue = 0; //value of the particular level
    private static float threshold = 0.01f;

    public static String index_folder;
    public static String globalPageRank = "globalPagerank.txt";
    public static String testFile = "testfile.txt";
    public static String backwardIndex = "backward-index.txt";
    public static String forwardLinkCount = "forwardCounts.txt";
    public static String backwardIndex_index = "backward-index-index.txt";
    public static int OFFSET;


    public static void readPageRankInMemory() {
        BufferedReader br = null;
        try {
            File file = new File(index_folder + globalPageRank);
            br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                String tok[] = line.split(":");
                int val1 = Integer.parseInt(tok[0]);
                float val2 = Float.parseFloat(tok[1]);
                pagerank[val1] = val2;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void readForwardIndexCountsInMemory() {

        BufferedReader br = null;
        try {
            File file = new File(index_folder + forwardLinkCount);

            br = new BufferedReader(new FileReader(file));

            forward_count_map = new HashMap<Integer, Integer>();
            String line;
            while ((line = br.readLine()) != null) {
                String tok[] = line.split(":");
                forward_count_map.put(Integer.parseInt(tok[0]),
                        Integer.parseInt(tok[1]));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void readBackwardIndexIndexInMemory() {

        BufferedReader br = null;

        try {
            File file = new File(index_folder + backwardIndex_index);
            br = new BufferedReader(new FileReader(file));

            /*back_index_map = new HashMap<Integer, List<Integer>>();
            String line;
            while ((line = br.readLine()) != null) {
                String tok[] = line.split(":");
                String temp[] = tok[1].split(",");

                List<Integer> param = new ArrayList<Integer>();

                for (String s : temp) {
                    param.add(Integer.parseInt(s));
                }

                back_index_map.put(Integer.parseInt(tok[0]), param);
            }*/
            String line;

            while ((line = br.readLine()) != null) {
                    String tok[]  = line.split(":");
                    back_index_index[Integer.parseInt(tok[0])] = Long.parseLong(tok[1]);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static List<Integer> getBackWardindexList(int pageId){

        List<Integer> tempList = new ArrayList<Integer>();
        File fty = new File(index_folder + backwardIndex);
        RandomAccessFile raf = null;
        try {
            raf = new RandomAccessFile(fty , "r");
            raf.seek(back_index_index[pageId]);
            String line = raf.readLine();
            //System.out.println(raf.readLine());
            int colonIndex = line.indexOf(":");
            String links = line.substring(colonIndex + 1, line.length());
            String [] tok = links.split(",");
            for(String st : tok){
                tempList.add(Integer.parseInt(st));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                raf.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return tempList;
    }


    public static void main(String arr[]){
            index_folder = arr[0];
        try {

            System.out.println("Loading data in Memory");
            readPageRankInMemory();
            System.out.println("Global Page Rank in Memory");
            readForwardIndexCountsInMemory();
            System.out.println("Forward Index Counts In Memory");
            readBackwardIndexIndexInMemory();
            System.out.println("Backward Index's index is In Memory");
            System.out.println("...Loaded!!");

            //Input an Id
            System.out.println("PageId\tApproxPageRank\tGlobalPageRank\tRelativeError\tPrecision");
            Scanner in=new Scanner(System.in);
            while(true) {
            //For manual input uncomment above three lines and comment below three lines

            ///BufferedReader br = new BufferedReader(new FileReader(new File(index_folder + testFile)));
            //String queryPid;
            //while ( (queryPid = br.readLine()) != null ) {

                levelid=0;
                lastidvalue=0;

                String queryPid = in.nextLine();
                float queryPagerank = 0f;
                List<Integer> localqueue;

                //localqueue = back_index_map.get(Integer.parseInt(queryPid));----------
                localqueue =  getBackWardindexList(Integer.parseInt(queryPid));
                lastidvalue = localqueue.get((localqueue.size() - 1));
                //br.write(localqueue + "\n");

                levelid++;

                List<HashMap<Integer, Float>> levelInfluenceMaplist = new ArrayList<HashMap<Integer, Float>>();
                HashMap<Integer, Float> tempLevelInfluenceMap = new HashMap<Integer, Float>();
                int pruned_count = 0;
                int level_1_count = 0;
                for (Integer val : localqueue) {
                    float inf = 1.0f / forward_count_map.get(val);
                    //float prune = 0.85f * inf;
                    //if(prune > threshold) {
                        tempLevelInfluenceMap.put(val, inf);
                    //}else{
                    //    pruned_count++;
                    //}
                    //br.write("Influnce:" + val + "--" + inf + "\n");
                    level_1_count++;
                }
                //System.out.println(level_1_count + " " + pruned_count);
                for (Map.Entry<Integer, Float> entry : tempLevelInfluenceMap.entrySet()) {
                    queryPagerank += 0.15 * Math.pow(0.85, levelid)* entry.getValue();
                }
                levelInfluenceMaplist.add(tempLevelInfluenceMap);

                while (true) {
                    //System.out.println(levelid);
                    if (levelid == radius) {
                        break;
                    }
                    int temp = localqueue.get(0);
                    //List<Integer> templist = back_index_map.get(temp);-----
                    List<Integer> templist = getBackWardindexList(temp);
                    if (templist != null) {
                        //localqueue.addAll(back_index_map.get(temp));
                        localqueue.addAll(getBackWardindexList(temp));
                    }
                    localqueue.remove(0);
                    if (temp == lastidvalue) {
                        levelid++;
                        lastidvalue = localqueue.get((localqueue.size() - 1));

                        HashMap<Integer, Float> tempMap = levelInfluenceMaplist.get(levelInfluenceMaplist.size() - 1);
                        tempLevelInfluenceMap = new HashMap<Integer, Float>();
                        for (Map.Entry<Integer, Float> entry : tempMap.entrySet()) {
                            //List<Integer> tlist = back_index_map.get(entry.getKey());
                            List<Integer> tlist = getBackWardindexList(entry.getKey());
                            if (tlist == null)
                                continue;
                            for (int val : tlist) {
                                float newinf = (1.0f / forward_count_map.get(val)) * entry.getValue();
                                //float prune =  (float)Math.pow(0.85, levelid) * newinf;
                                //if(prune > threshold) {
                                    ///pruning,...
                                    if (tempLevelInfluenceMap.get(val) == null)

                                        tempLevelInfluenceMap.put(val, newinf);
                                    else {
                                        float tempinf = tempLevelInfluenceMap.get(val);
                                        newinf += tempinf;
                                        tempLevelInfluenceMap.put(val, newinf);
                                    }
                                //}
                                //br.write("Influnce:" + val + "--" + newinf+ "\n");
                            }
                            tlist.clear();
                        }
                        tempMap.clear();
                        levelInfluenceMaplist.add(tempLevelInfluenceMap);
                        float tempqueryPagerank = 0f;
                        for (Map.Entry<Integer, Float> entry : tempLevelInfluenceMap.entrySet()) {

                            tempqueryPagerank += 0.15f * Math.pow(0.85, levelid)* entry.getValue();
                        }
                        queryPagerank += tempqueryPagerank;
                        tempLevelInfluenceMap.clear();
                    }
                }

                float globalPageRank = pagerank[Integer.parseInt(queryPid)];
                float relativeError =  Math.abs(queryPagerank - globalPageRank) / globalPageRank;
                float precision = queryPagerank / globalPageRank ;
                System.out.println(queryPid+"\t"+queryPagerank+"\t"+globalPageRank+"\t"+relativeError+"\t"+precision);
                //System.out.println("PageId : " + queryPid);
                //System.out.println("NewPageRank:" + queryPid + "-->"  + queryPagerank);
                //System.out.println("OldPageRank:" + queryPid + "-->" + globalPageRank);
                //System.out.println("Relative Error :" + relativeError);
                //System.out.println("Precision :" + precision);

                localqueue.clear();
                levelInfluenceMaplist.clear();
            }

    } catch (Exception e) {
        e.printStackTrace();
    }
}

}
