Local Approximation of Page Rank
================================

This is a repository containing the source code for the Major Project for
IIIT Hyderabad's CSE474 (Information Retrieval and Extraction).

The project implements a local approximation of the well known page rank
algorithm.

How to run Project ?
====================

---cd src

Index creation:

---bash index.sh < path to wikipedia corpus xml file >

PageRank and Local Approximation of Pagerank generation:

---javac *.java
---java LAPageRankMain < folder where index is created > 
---java LocalApproxPageRank < folder where index is created >


Contributors
------------

In alphabetical order:

1. Anhad Jai Singh
2. Jigar Kaneriya
3. Rishi Mittal
4. Sandeep Reddy Biddala
