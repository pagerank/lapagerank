import Queue
import heapq
out_filename = 'merged.txt'

def keyfunc(line):
    return int(line.split(':',1)[0])

def valuefunc(line):
    return line.split(':',1)[1]

def read_file(f, key):
    for line in f:
        yield (key(line), line[:-1])

def merge(file_name,n):
    fds=[]
    prio_q = Queue.PriorityQueue()
    for i in range(n):
        fds.append( open(file_name+str(i+1)+'.txt') )
    old = ''
    outfile = open(out_filename,'w')
    for line in heapq.merge(*[read_file(f, keyfunc) for f in fds]):
        if old == '':
            old = line[1]
        elif keyfunc(old) == keyfunc(line[1]):
            old += valuefunc(line[1])
        else:
            outfile.write(old+'\n')
            old = line[1]
    outfile.write(old)



#main
#if len(argv) != 3:
#    sys.exit()
#merge('inverted_index_',4)
