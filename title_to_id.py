import sys
import HTMLParser


def title_to_id(file_name):
    """
    This function maps title(stdin) to its id(stdout)
    :param fileName:name of file which contains mapping in format of TitleIds Title name
    """
    title_del = "t:"
    link_del = "l:"
    d = {}
    parser=HTMLParser.HTMLParser()
    for s in open(file_name, 'r'):
        a = s.split(" ", 1)
        d[a[1].lower()] = a[0]
    #line = str(parser.unescape(parser.unescape(sys.stdin.readline())))
    line = sys.stdin.readline()
    line = line.lower()
    line_no = 1
    chance = True
    while line:
        if line.startswith(link_del):
            index_of_deli = line.find('|')
            index_of_hash = line.find('#')
            if index_of_hash != -1:
                if index_of_deli != -1:
                    index_of_deli = min(index_of_hash, index_of_deli)
                else:
                    index_of_deli = index_of_hash
            if index_of_deli == -1:
                if d.get(line[2:], False):
                    sys.stdout.write(d[line[2:]] + ",")
                elif chance:
                    try:
                        line = str(parser.unescape(parser.unescape(line)))
                        chance = False
                        continue
                    except Exception:
                        sys.stderr.write(str(line_no) + ":" + line)
                else:
                    sys.stderr.write(str(line_no) + ":" + line)
            else:
                if d.get(line[2:index_of_deli] + "\n", False):
                    sys.stdout.write(d.get(line[2:index_of_deli] + "\n") + ",")
                elif chance:
                    try:
                        line = str(parser.unescape(parser.unescape(line)))
                        chance = False
                        continue
                    except Exception:
                        sys.stderr.write(str(line_no) + ":" + line)
                else:
                    sys.stderr.write(str(line_no) + ":" + line)
                #else:
                #    sys.stderr.write(str(line_no) + ":" + line)
        elif line.startswith(title_del):
            if d.get(line[2:], False):
                sys.stdout.write("\n" + d[line[2:]] + ":")
            elif chance:
                try:
                    line = str(parser.unescape(parser.unescape(line)))
                    chance = False
                    continue
                except Exception:
                    sys.stderr.write(str(line_no) + ":" + line)
            else:
                sys.stderr.write(str(line_no) + ":" + line)
            #else:
            #    sys.stderr.write(str(line_no) + ":" + line)
        else:
            sys.stderr.write(str(line_no) + ":" + line)
        #line = str(parser.unescape(parser.unescape(sys.stdin.readline())))
        #print line
        chance = True
        line = sys.stdin.readline()
        line = line.lower()
        line_no += 1


if len(sys.argv) < 2:
    print("Usage: python titleToId.py <filename>\nfile contains <titleid title\\n>")
    sys.exit(0)

title_to_id(sys.argv[1])
