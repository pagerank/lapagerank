import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by rishimittal on 23/3/14.
 */

public class PageRank {

    //private static Map<Integer , Float> myMap  = new HashMap<Integer, Float>(15000000);
    //private static Map<Integer , Integer > countMap = new HashMap<Integer, Integer>(15000000);

    public static float [] myArray = new float[50000000];
    public static int [] countArray = new int[50000000];


    private static boolean calculatePageRank(String line) {

        String [] lineSplit = line.split(":");
        int pageId = Integer.parseInt(lineSplit[0]);

        String [] pageSplit = lineSplit[1].split(",");

        float tempRank = 0;
        int tempFreq = 0;

        for(int i = 0 ; i  < pageSplit.length ; i++ ){
            //System.out.println(pageSplit[i]);

            //Float tempRf = myMap.get(Integer.parseInt(pageSplit[i]));
            //int freq = countMap.get(Integer.parseInt(pageSplit[i]));
            Float tempRf = myArray[Integer.parseInt(pageSplit[i])];
            int freq = countArray[Integer.parseInt(pageSplit[i])];

            if(tempRf == null){
                tempRank = (float) (tempRank +  1 / (float)freq );
            }else{
                tempRank = (float) ( tempRank + tempRf / (float)freq) ;
            }
        }
        float val = (float)(tempRank * 0.85 + 0.15);

        float oldPageRank = myArray[pageId];
        myArray[pageId] = val;
        return (oldPageRank == val);
    }

    public static int  readandIterate(){

        File f = new File("/home/rishimittal/workspace/index/merged.txt"); //filePath of the pageId->from links
        int count = 0;
        try {
            BufferedReader br = new BufferedReader(new FileReader(f));
            String line ;
            while( ( line  = br.readLine()  )!= null){
                //System.out.println(line);
                if(!calculatePageRank(line)){
                    count++;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
       return count;
    }

    public static void countInLinks(){

        File f = new File("/home/rishimittal/workspace/index/pageid_and_links.txt");//Filepath of the link->outlinks file
        int count = 0;
        try {
            BufferedReader br = new BufferedReader(new FileReader(f));
            String line ;
            br.readLine();
            while( ( line  = br.readLine()  )!= null){
                String []ids = line.split(":");
                if(ids.length == 2 ) {
                    String []ifs = ids[1].split(",");
                    //countMap.put(Integer.parseInt(ids[0]),ifs.length);
                    //myMap.put(Integer.parseInt(ids[0]), (float)1 / 10);
                    countArray[Integer.parseInt(ids[0])] = ifs.length;
                    myArray[Integer.parseInt(ids[0])] = (float) 0.1;
                    System.out.println(count++);
                }else{
                    continue;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Counting Complete");
    }

    public static void main(String arr[]) {


        countInLinks();
        int count = 99999;
        int loopCount = 0;
        try {
        int d = 20;
         //while(count != 0) {  //Used when convergence is required
            while(d != 0 ) {
            loopCount++;
            System.out.println(loopCount);
            count = readandIterate();
            File fd = new File("/home/rishimittal/workspace/index/output");
            PrintWriter pw = new PrintWriter(new FileWriter(fd));
            //Print the pageRank
            for(int i = 0 ; i < 50000000 ; i++ ){
                pw.write(i + "  " + myArray[i]);
                pw.write("\n");
            }
            pw.flush();
            pw.close();
            d--; //remove when convergence is used.
        }

        System.out.println(loopCount);
        } catch (IOException e) {
            e.printStackTrace();
        }
     }


}
